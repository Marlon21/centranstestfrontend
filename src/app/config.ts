import { Injectable } from '@angular/core'
@Injectable()
export class Config {

  //credenciales pruebas
  //public static get API_ENDPOINT(): string { return 'http://190.111.13.29:8084/PioWorld2/'; }
  public static get API_ENDPOINT_MAIN(): string { return 'http://190.111.13.29:8084/'; }

  public static get API_ENDPOINT(): string { return 'http://localhost:8081/api/'; }
  //public static get API_ENDPOINT_MAIN(): string { return 'http://190.111.13.29:8084/'; }

  //public static get API_ENDPOINT_BOXPRO(): string { return 'https://mia.cpsworldwide.com:8085/api/'; }
  public static get API_ENDPOINT_BOXPRO(): string { return 'http://192.168.156.132:8081/api/'; }
  
}
