import { AfterViewInit, Component, Inject, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Globals } from '../shared/Globals';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import { DatePipe } from '@angular/common';
import { ControlsService } from '../services/controls.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { DataSource } from '@angular/cdk/collections';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Router } from '@angular/router';

export interface RateData {
  id: string;
  date: string;
  rate: string;
  username: string;
}

@Component({
  providers: [DatePipe],
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements AfterViewInit {
  displayedColumns: string[] = ['date', 'rate', 'username', 'actions'];
  dataSource: MatTableDataSource<RateData>;

  @ViewChild(MatPaginator)
  paginator!: MatPaginator;
  @ViewChild(MatSort)
  sort!: MatSort;
  
  date = new FormControl(new Date());
  serializedDate = new FormControl((new Date()).toISOString());
  spinner:boolean = false;
  spinnerActions:boolean = false;
  profileForm = new FormGroup({
    firstName: new FormControl(''),
    lastName: new FormControl(''),
  });
  constructor(private globals: Globals, public datePipe: DatePipe, private snackBar: MatSnackBar, 
              private controlService: ControlsService, public dialog: MatDialog,
              private router: Router) { 
    this.dataSource = new MatTableDataSource();
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }
  
  ngOnInit() {
    if (!this.globals.isLoggin())
    {
      this.router.navigate(['/login']);
    }
    this.globals.showMenu.emit(true);
    this.getRates();
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  getExchangeRate() {
    this.spinner = true;
    let setDate = this.datePipe.transform(this.date.value, 'dd/MM/yyyy');

    this.controlService.find("rate/getRateToBank", setDate)
      .subscribe((res: any) => {
        if (res) {
          this.snackBar.open('Tipo de cambio guardado correctamente', 'Close', {
            duration: 5000,
          });
          this.getRates();
        } else {
          this.snackBar.open('No se pudo obtener el tipo de cambio', 'Close', {
            duration: 5000,
        });
        }
        this.spinner = false;
        //this.activate = false;
      }, (error: any) => {
          const status = error.status;
          this.spinner = false;
          //this.activate = false;
          if (status === 400) {
          this.snackBar.open('Ocurrió un error', 'Close', {
              duration: 5000,
          });
          }
      });
  }

  getRates() {
    this.controlService.find("rate/getRates", "")
      .subscribe((res: any) => {
        this.dataSource = new MatTableDataSource(res);
        console.log(res);
        //this.spinner = false;
      }, (error: any) => {
          const status = error.status;
         // this.spinner = false;
          if (status === 400) {
          this.snackBar.open('Incorrect User or Password', 'Close', {
              duration: 5000,
          });
          }
      });
  }

  editExchangeRate(row: any) {
    this.spinnerActions = true;
    this.controlService.updateRegister(row.id, row, "rate")
      .subscribe((res: any) => {
        if (res) {
          this.snackBar.open('Tipo de cambio editado correctamente', 'Close', {
            duration: 5000,
          });
        } else {
          this.snackBar.open('No se pudo editar el tipo de cambio', 'Close', {
            duration: 5000,
        });
        }
        this.spinnerActions = false;
      }, (error: any) => {
          const status = error.status;
          this.spinnerActions = false;
          if (status === 400) {
          this.snackBar.open('Ocurrió un error', 'Close', {
              duration: 5000,
          });
          }
      });
  }

  deleteExchangeRate(row: any) {
    this.spinnerActions = true;
    this.controlService.postSave("rate/deleteRate", row)
    .subscribe((res: any) => {
      this.dataSource = new MatTableDataSource(res);
      if(res) {
        this.snackBar.open('Tipo de cambio eliminado correctamente', 'Close', {
          duration: 5000,
        });
      } else {
        this.snackBar.open('No se pudo eliminar el tipo de cambio', 'Close', {
          duration: 5000,
        });
      }
      this.spinnerActions = false;
      this.getRates();
      console.log(res);
      this.spinnerActions = false;
    }, (error: any) => {
        const status = error.status;
        this.spinnerActions = false;
        if (status === 400) {
        this.snackBar.open('Ocurrió un error', 'Close', {
            duration: 5000,
        });
        }
    });
  }

  openEditDialog(row:any): void {
    const dialogRef = this.dialog.open(EditRateDialog, {
      width: '250px',
      data: row
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        row.rate = result;
        this.editExchangeRate(row);
      }
    });
  }

}

@Component({
  selector: 'edit-rate-dialog',
  templateUrl: 'editRateDialog.html',
})
export class EditRateDialog {

  public newRate = "";
  constructor(
    public dialogRef: MatDialogRef<EditRateDialog>,
    @Inject(MAT_DIALOG_DATA) public data: RateData) {}

  onNoClick(): void {
    this.dialogRef.close();
  }

}
