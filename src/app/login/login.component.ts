import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ControlsService } from '../services/controls.service';
import { Globals } from '../shared/Globals';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public model: any;
  public activate = false;
  public showBtnLoginLogout = false;
  constructor(public controlService: ControlsService, public globals: Globals, public snackBar: MatSnackBar,
              private router: Router, private http: HttpClient) {

    /*if (this.globals.isLoggin()) {
      this.router.navigate(['/movies']);
    }*/

      this.model = { 'Username': '', 'Password': '', 'remember_me': false };
  }

  ngOnInit() {
    if (this.globals.isLoggin()) {
      this.router.navigate(['/home']);
    }
    this.globals.showMenu.emit(false);
  }

  public login() {
    this.activate = true;
      this.controlService.login("Auth/token", this.model)
        .subscribe((res: any) => {
          this.activate = false;
          var session = { "username": res.username, "session": true };
          var token = res.token;
          localStorage.setItem("session", JSON.stringify(session));
          localStorage.setItem("token", JSON.stringify(token));
          this.router.navigate(['home']);
          this.globals.refreshOptionSession();
        }, (error: any) => {
            const status = error.status;
            this.activate = false;
            if (status === 400) {
            this.snackBar.open('Incorrect User or Password', 'Close', {
                duration: 5000,
            });
            }
        });
  }

}
