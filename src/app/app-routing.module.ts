import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { LogoutComponent } from './login/logout.component';
import { ShipsMapComponent } from './ships-map/ships-map.component';
import { UsersComponent } from './users/users.component';

const routes: Routes = [
  { path: '', component: LoginComponent, data: {toolbar: false}},
  { path: 'login', component: LoginComponent, data: {toolbar:false}},
  { path: 'home', component: HomeComponent},
  { path: 'ships-map', component: ShipsMapComponent, data: {toolbar: false}},
  { path: 'logout', component: LogoutComponent },
  { path: 'users', component: UsersComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
