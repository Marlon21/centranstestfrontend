import { Component, EventEmitter, ViewChild } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { map, filter, mergeMap } from "rxjs/operators";
import { MenuComponent } from './menu/menu.component';
import { Globals } from './shared/Globals';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'prueba';
  public visible: boolean;
  
  @ViewChild('menu')
  public menu!: MenuComponent;

  public showBtnMediaSmall = false;
  public showBtnLoginLogout = true;
  public username = '';
  public showUsername = false;

  constructor(private router: Router, private activatedRoute: ActivatedRoute, private globals: Globals) {
    this.visible = true;
  }

  ngOnInit() {
    this.router.events
      .pipe(
        filter(event => event instanceof NavigationEnd),
        map(() => this.activatedRoute),
        map(route => {
          while (route.firstChild) {
            route = route.firstChild;
          }
          return route;
        }),
      )
      .pipe(
        filter(route => route.outlet === 'primary'),
        mergeMap(route => route.data),
      )
      .subscribe(event => {
        this.showToolbar(event.toolbar); // show the toolbar?
      });

      var me = this;
        this.globals.showUsername = new EventEmitter<any>();
        this.globals.showUsername.subscribe(e => {
          if (e) {
            const session: any = localStorage.getItem("session");
            const val = JSON.parse(session);
            this.username = val.username;
          }
          setTimeout(function () {
            me.showUsername = e;
          }, 200);
        });
        this.globals.refreshOptionSession();
  }

  showToolbar(event: any) {
    if (event === undefined) {
      this.visible = true;
    }
    if (event === false) {
      this.visible = false;
    } else if (event === true) {
      this.visible = true;
    } else {
      this.visible = this.visible;
    }
  }

  public showBtnMediaSmallFn(event:any) {
    this.showBtnMediaSmall = event;
  }
  public redirectMain() {
      //this.globals.refreshOptionSession();
      this.router.navigate(['/login']);
      /*if (this.globals.isLoggin()) {
          this.router.navigate(['/admin/users']);
      }*/
  }
  ngAfterViewInit(): void {
      //this._changeDetectionRef.detectChanges();
  }

}
