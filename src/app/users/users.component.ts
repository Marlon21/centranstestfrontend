import { DatePipe } from '@angular/common';
import { AfterViewInit, Component, Inject, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { ControlsService } from '../services/controls.service';
import { Globals } from '../shared/Globals';

export interface UserData {
  name: string;
  lastName: string;
  email: string;
  userName: string;
  PhoneNumber: string;
}

export interface NewUser {
  Name: string;
  LastName: string;
  Email: string;
  Username: string;
  PhoneNumber: string;
  Password: string;
  ConfirmPassword: string;
}

@Component({
  providers: [DatePipe],
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements AfterViewInit {
  displayedColumns: string[] = ['name', 'email', 'userName', 'phoneNumber' ,'actions'];
  dataSource: MatTableDataSource<UserData>;

  @ViewChild(MatPaginator)
  paginator!: MatPaginator;
  @ViewChild(MatSort)
  sort!: MatSort;
  serializedDate = new FormControl((new Date()).toISOString());
  spinner:boolean = false;
  spinnerActions:boolean = false;
  fullName: string = "";
  profileForm = new FormGroup({
    firstName: new FormControl(''),
    lastName: new FormControl(''),
  });
  constructor(private globals: Globals, public datePipe: DatePipe, private snackBar: MatSnackBar, 
    private controlService: ControlsService, public dialog: MatDialog,
    private router: Router) { 
      this.dataSource = new MatTableDataSource();
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }
  
  ngOnInit() {
    if (!this.globals.isLoggin())
    {
      this.router.navigate(['/login']);
    }
    this.globals.showMenu.emit(true);
    this.getUsers();
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  getUsers() {
    this.controlService.find("user/getUsers", "")
      .subscribe((res: any) => {
        this.dataSource = new MatTableDataSource(res);
        console.log(res);
        //this.spinner = false;
      }, (error: any) => {
          const status = error.status;
          this.spinner = false;
          if (status === 400) {
          this.snackBar.open('Incorrect User or Password', 'Close', {
              duration: 5000,
          });
          }
      });
  }

  addUser(row: any) {
    this.spinner = true;
    this.controlService.postSave("Auth/register", row)
    .subscribe((res: any) => {
      this.dataSource = new MatTableDataSource(res);
      if(res) {
        this.snackBar.open('Usuario Creado Correctamente', 'Close', {
          duration: 5000,
        });
      } else {
        this.snackBar.open('No se pudo crear el usuario', 'Close', {
          duration: 5000,
        });
      }
      this.spinner = false;
      this.getUsers();
      console.log(res);
      this.spinner = false;
    }, (error: any) => {
        const status = error.status;
        this.spinner = false;
        if (status === 400) {
        this.snackBar.open(error.error.errorMessage, 'Close', {
            duration: 5000,
        });
        }
    });
  }

  editUser(row: any) {
    this.spinnerActions = true;
    this.controlService.updateRegister(row.id, row, "rate")
      .subscribe((res: any) => {
        if (res) {
          this.snackBar.open('Tipo de cambio editado correctamente', 'Close', {
            duration: 5000,
          });
        } else {
          this.snackBar.open('No se pudo editar el tipo de cambio', 'Close', {
            duration: 5000,
        });
        }
        this.spinnerActions = false;
      }, (error: any) => {
          const status = error.status;
          this.spinnerActions = false;
          if (status === 400) {
          this.snackBar.open('Ocurrió un error', 'Close', {
              duration: 5000,
          });
          }
      });
  }

  deleteUser(row: any) {
    this.spinnerActions = true;
    this.controlService.postSave("rate/deleteRate", row)
    .subscribe((res: any) => {
      this.dataSource = new MatTableDataSource(res);
      if(res) {
        this.snackBar.open('Tipo de cambio eliminado correctamente', 'Close', {
          duration: 5000,
        });
      } else {
        this.snackBar.open('No se pudo eliminar el tipo de cambio', 'Close', {
          duration: 5000,
        });
      }
      this.spinnerActions = false;
      this.getUsers();
      console.log(res);
      //this.spinner = false;
    }, (error: any) => {
        const status = error.status;
        this.spinnerActions = false;
        if (status === 400) {
        this.snackBar.open('Ocurrió un error', 'Close', {
            duration: 5000,
        });
        }
    });
  }

  openAddDialog(): void {
    var row = {}
    const dialogRef = this.dialog.open(AddUserDialog, {
      width: '500px',
      data: row
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.addUser(result);
      }
    });
  }

  openEditDialog(row:any): void {
    const dialogRef = this.dialog.open(EditUserDialog, {
      width: '250px',
      data: row
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        row.rate = result;
      }
    });
  }
}

@Component({
  selector: 'add-user-dialog',
  templateUrl: 'addUserDialog.html',
})
export class AddUserDialog {

  public newRate = "";
  constructor(
    public dialogRef: MatDialogRef<AddUserDialog>,
    @Inject(MAT_DIALOG_DATA) 
    public data: NewUser) {}

  onNoClick(): void {
    this.dialogRef.close();
  }
}

@Component({
  selector: 'edit-user-dialog',
  templateUrl: 'editUserDialog.html',
})
export class EditUserDialog {

  public newRate = "";
  constructor(
    public dialogRef: MatDialogRef<EditUserDialog>,
    @Inject(MAT_DIALOG_DATA) public data: UserData
    ) {}

  onNoClick(): void {
    this.dialogRef.close();
  }
}
