import { AfterContentInit, AfterViewInit, Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { MatDrawerMode, MatSidenav } from '@angular/material/sidenav';
import { ActivatedRoute, Router } from '@angular/router';
import { Globals } from '../shared/Globals';
import { MediacheckService } from '../services/mediacheck.service';
@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements AfterViewInit {

  public menuList: any[] = [];
  public opened = false;
  public step = 0;
  public mode: MatDrawerMode = 'side';

  @Output()
  private _showBtnMediaSmall: EventEmitter<any> = new EventEmitter<any>();
  public get showBtnMediaSmall(): EventEmitter<any> {
    return this._showBtnMediaSmall;
  }
  public set showBtnMediaSmall(value: EventEmitter<any>) {
    this._showBtnMediaSmall = value;
  }

  @ViewChild(MatSidenav)
  sidenav!: MatSidenav;

  constructor(public globals: Globals, public router: Router, public mc: MediacheckService, private route: ActivatedRoute) {

  }
  public setStep(index: number) {
      this.step = index;
  }

  ngAfterViewInit(){
      var me = this;
      //console.log(this.route.url);
      //console.log(this.router.url);
      if (this.globals.isLoggin()) {
          if (this.mc.check('small')) {
              this.sidenav.mode = "over";
              setTimeout(function () {
                  me.showBtnMediaSmall.emit(true);
              }, 500);
          } else {
              this.sidenav.mode = "side";
              this.opened = true;
              setTimeout(function () {
                  me.showBtnMediaSmall.emit(false);
              }, 500);
          };            
      }
      else {
          this.opened = false;
        }
      this.mc.onMqChange('small', (mql: any) => {
          if (me.globals.isLoggin()) {
              setTimeout(function () {
                  me.sidenav.mode = "over";
                  me.showBtnMediaSmall.emit(true);
                  me.sidenav.close();
              }, 200);
          }
          
      });

      this.mc.onMqChange('large', (mql: any) => {
          if (me.globals.isLoggin()) {
              me.sidenav.close();
              setTimeout(function () {
                  me.sidenav.mode = "side";
                  me.showBtnMediaSmall.emit(false);
                  me.sidenav.open();
              }, 500);
          }            
      });       
      this.globals.showMenu = new EventEmitter<any>();

      this.globals.showMenu.subscribe((e: any) => {
          //console.log(e);
          if (me.globals.isLoggin()) {
              if (e === false) {
                  setTimeout(function () {
                      me.sidenav.close();
                      me.showBtnMediaSmall.emit(false);
                  }, 200);
                  //me.opened = false;
              } else {
                  //console.log('loadData2');
                  if (me.mc.check('small')) {                        
                      setTimeout(function () {
                          me.sidenav.mode = "over";
                          me.showBtnMediaSmall.emit(true);
                      }, 200);
                  } else {                        
                      //me.opened = true;
                      setTimeout(function () {
                          me.sidenav.mode = "side";
                          me.showBtnMediaSmall.emit(false);
                          me.sidenav.open();
                      }, 200);
                      
                  };
              }
          } else {                
              setTimeout(function () {
                  me.sidenav.close();
                  me.showBtnMediaSmall.emit(false);
              }, 200);
          }
          
      });
      
  }
  
  public openMenu() {
    //console.log(this.sidenav.opened);
    if (this.sidenav.opened) {
      this.sidenav.close();
    } else {
      this.sidenav.open();
    }
      
  }
}
