import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {
  CommonModule
} from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { FormsModule } from '@angular/forms';
import { MaterialModule } from './material.module';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { EditRateDialog, HomeComponent } from './home/home.component';
import { ShipsMapComponent } from './ships-map/ships-map.component';
import { AgmCoreModule } from '@agm/core';
import { MatIconModule } from '@angular/material/icon';
import { LoginComponent } from './login/login.component';
import { ControlsService } from './services/controls.service';
import { Globals } from './shared/Globals';
import { MenuComponent } from './menu/menu.component';
import { MediacheckService } from './services/mediacheck.service';
import { AddUserDialog, EditUserDialog, UsersComponent } from './users/users.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ShipsMapComponent,
    LoginComponent,
    MenuComponent,
    EditRateDialog,
    AddUserDialog,
    EditUserDialog,
    UsersComponent
  ],
  imports: [
    BrowserModule,
    CommonModule,
    MaterialModule,
    AppRoutingModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpClientModule,
    MatIconModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyCG8rSKnrGmVSosUl9SZNHlOFMaCh8TLHk'
    })
  ],
  providers: [ControlsService, Globals, MediacheckService],
  bootstrap: [AppComponent],
  entryComponents: [EditRateDialog, AddUserDialog, EditUserDialog],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule { }
