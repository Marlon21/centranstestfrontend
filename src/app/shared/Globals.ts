import { Injectable, ViewChild, Output, EventEmitter  } from '@angular/core'
import { Observable } from 'rxjs/';
import { BehaviorSubject } from 'rxjs/';
@Injectable()
export class Globals {
    public usernameShow: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
    @Output()
    public showMenu: EventEmitter<any> = new EventEmitter;
    public showUsername: EventEmitter<any> = new EventEmitter;
    //public showMenuMedia: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

    constructor() {

    }
    
    public setUsernameShow(UsernameShow: boolean) {
      this.usernameShow.next(UsernameShow);
    }

    public getUsernameShow(): boolean {
      return this.usernameShow.value;
    }

    public refreshOptionSession(){
        var session = localStorage.getItem("session");
        //console.log(session);
        
        if (session !== null && session!=="") {
            var val = JSON.parse(session);
            if (val.session) {
                this.showUsername.emit(true);
            } else {
                this.showUsername.emit(false);
            }           
        } else {
            var sessionVal = { "username": "", "session": false };
            localStorage.setItem("session", JSON.stringify(sessionVal));
            localStorage.setItem("token", "");
            this.showUsername.emit(false);
        }
    }
    public getBtnLogout(): boolean {
        //return this.btnLogout.value;
        return false;
    }
    
    public getToken():string {
        var session = localStorage.getItem("session");
        var token = localStorage.getItem("token");
        var me = this;
        if ((session !== null && session !== "") && (token !== null && token !== "")) {
            var val = JSON.parse(session);
            var tok = JSON.parse(token);
            if (val.session) {
                return tok;
            } else {
                return "";
            }
        } else {
            return "";
        }
    }
    public isLoggin(): boolean {
        var session = localStorage.getItem("session");
        var token = localStorage.getItem("token");
        var me = this;
        //console.log(session);
        //console.log(token);
        if ((session !== null && session !== "") && (token !== null && token !== "")) {
            var val = JSON.parse(session);
            var tok = JSON.parse(token);
            if (val.session===true) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
}

/*@Injectable()
export class Globals {
    public btnLoginShow: Observable<boolean>;

    constructor() {

    }
    public setBtnLoginShow(BtnLoginShow: boolean) {
        this.btnLoginShow = Observable.of(BtnLoginShow).share();
    }

    public getBtnLoginShow(): Observable<boolean> {
        return this.btnLoginShow;
    }
}*/

