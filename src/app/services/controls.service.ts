import { Injectable } from '@angular/core'
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs'
import 'rxjs/'
import { Config } from '../config'
import { Router, RouterModule } from '@angular/router'
import { Globals } from '../shared/Globals';
import { HttpClientModule } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable()
export class ControlsService {

  constructor(private httpC: HttpClient, public globals: Globals, public router: Router) {

  }
  public login(url: string, param: any) {
      var header = new HttpHeaders().set('Content-Type', 'application/json; charset=UTF-8').set('Accept', 'application/json');
      return this.httpC.post(Config.API_ENDPOINT + url, param, { headers: header, responseType: 'json' })
        .pipe(map((res) => res));
  }

  public signOn(url: string, param: any) {
    var header = new HttpHeaders().set('Content-Type', 'text/xml; charset=UTF-8').set('Accept', 'application/xml');
    return this.httpC.post(Config.API_ENDPOINT + url, param, { headers: header, responseType: 'text' })
      .pipe(map((res) => res));
  }
  public postFind(url: string, param: any) {
    var header = new HttpHeaders().set('Content-Type', 'text/xml; charset=UTF-8').set('Accept', 'application/xml');
    return this.httpC.post(Config.API_ENDPOINT + url, param, { headers: header, responseType: 'text' })
      .pipe(map((res) => res));
  }

  public postSave(url: string, param: any) {
    var token = this.globals.getToken();
    var authorization = 'bearer ' + token;
    var header = new HttpHeaders().set('Content-Type', 'application/json; charset=UTF-8')
                .set('Accept', 'application/json')
                .set('Authorization', authorization);
    return this.httpC.post(Config.API_ENDPOINT + url, param, { headers: header, responseType: 'json' })
      .pipe(map((res) => res));
  }

  public find(url: string, param:any) {
    var token = this.globals.getToken();
    var authorization = 'bearer ' + token;
    var header = new HttpHeaders().set('Access-Control-Allow-Origin', '*')
                                  .set("Authorization", authorization);
    return this.httpC.get(Config.API_ENDPOINT + url + '?date=' + param, {headers: header})
    .pipe(map((res) => res));
  }

  public updateRegister(id: number, register: any, url: string) {
    var token = this.globals.getToken();
      var authorization = 'bearer ' + token;
      var header = new HttpHeaders().set('Access-Control-Allow-Origin', '*')
                                  .set("Authorization", authorization);
      return this.httpC.put(Config.API_ENDPOINT + url+'/' + id, register, { headers: header })
      .pipe(map((res) => res));
}

}