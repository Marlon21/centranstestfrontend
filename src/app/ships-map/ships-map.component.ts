import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { MouseEvent } from '@agm/core';
import { Globals } from '../shared/Globals';

@Component({
  selector: 'app-ships-map',
  templateUrl: './ships-map.component.html',
  styleUrls: ['./ships-map.component.css']
})
export class ShipsMapComponent implements OnInit {

  // google maps zoom level
  zoom: number = 7;
  
  // initial center position for the map
  lat: number = 15.9352066;
  lng: number = -90.0403224;

  public icon:any = {
    url: '../assets/ship.png'
  }


  constructor(private globals: Globals) { }

  ngOnInit() {
    this.globals.showMenu.emit(false);
  }
  clickedMarker(label: string, index: number) {
    console.log(`clicked the marker: ${label || index}`)
  }
  
  markerDragEnd(m: marker, $event: MouseEvent) {
    console.log('dragEnd', m, $event);
  }
  
  markers: marker[] = [
    {
      lat: 14.683030,
      lng: -92.840839,
      label: 'A',
      draggable: true,
      description: "ship 1"
    },
    {
      lat: 14.948556, 
      lng: -94.334979,
      label: 'B',
      draggable: false,
      description: "ship 2"
    },
    {
      lat: 15.446853, 
      lng: -93.434101,
      label: 'C',
      draggable: true,
      description: "ship 3"
    },
    {
      lat: 13.339973, 
      lng: -93.862567,
      label: 'C',
      draggable: true,
      description: "ship 4"
    },
    {
      lat: 15.721959, 
      lng: -94.335871,
      label: 'C',
      draggable: true,
      description: "ship 5"
    },
    {
      lat: 14.597955,
      lng: -93.775568,
      label: 'C',
      draggable: true,
      description: "ship 6"
    },
    {
      lat: 13.628385,
      lng: -93.622215,
      label: 'C',
      draggable: true,
      description: "ship 7"
    },
    {
      lat: 13.660413,
      lng: -92.117088,
      label: 'C',
      draggable: true,
      description: "ship 8"
    },
    {
      lat: 14.512881,
      lng: -94.501121,
      label: 'C',
      draggable: true,
      description: "ship 9"
    },
    {
      lat: 13.905823,
      lng: -94.116599,
      label: 'C',
      draggable: true,
      description: "ship 10"
    },
    {
      lat: 16.123416,
      lng: -88.502586,
      label: 'C',
      draggable: true,
      description: "ship 11"
    },
    {
      lat: 16.070638,
      lng: -87.909324,
      label: 'C',
      draggable: true,
      description: "ship 12"
    },
    {
      lat: 16.439787,
      lng: -88.304832,
      label: 'C',
      draggable: true,
      description: "ship 13"
    },
    {
      lat: 17.028963,
      lng: -87.832420,
      label: 'C',
      draggable: true,
      description: "ship 14"
    },
    {
      lat: 16.724082,
      lng: -87.316063,
      label: 'C',
      draggable: true,
      description: "ship 15"
    },
    {
      lat: 16.418712,
      lng: -87.162254,
      label: 'C',
      draggable: true,
      description: "ship 16"
    },
    {
      lat: 17.671255,
      lng: -86.846397,
      label: 'C',
      draggable: true,
      description: "ship 17"
    },
    {
      lat: 17.063100,
      lng: -86.703575,
      label: 'C',
      draggable: true,
      description: "ship 18"
    }
  ]
}

// just an interface for type safety.
interface marker {
  lat: number;
  lng: number;
  label?: string;
  draggable: boolean;
  description: string;
}
